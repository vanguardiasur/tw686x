ifneq ($(KERNELRELEASE),)
obj-m := tw686x.o
tw686x-y := tw686x-core.o tw686x-video.o tw686x-audio.o
else
KDIR ?= /lib/modules/`uname -r`/build
build: genver
	$(MAKE) -C $(KDIR) M=$$PWD
genver:
	@echo "#define TW686X_GIT_HEAD \""$(shell git describe --abbrev=10 --dirty --always --tags)"\"" > tw686x-ver.h
install: build
	$(MAKE) -C $(KDIR) M=$$PWD modules_install
clean:
	$(MAKE) -C $(KDIR) M=$$PWD clean
endif
